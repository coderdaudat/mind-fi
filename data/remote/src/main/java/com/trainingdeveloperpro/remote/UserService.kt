package com.trainingdeveloperpro.remote

import com.trainingdeveloperpro.model.ApiResult
import com.trainingdeveloperpro.model.User
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface UserService {

    @GET("users")
    fun fetchTopUsersAsync(): Deferred<List<User>>

    @GET("users/{login}")
    fun fetchUserDetailsAsync(@Path("login") login: String): Deferred<User>
}